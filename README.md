# Level5 Technical Test

Emoji calculator to demonstrate basic knowledge of PHP.
The app was developed using the latest version of the Zend framework.

Two routes were implemented, one for the main view and another one to handle the ajax post call to calculate the results and return a json string to the main view.

## Approach

 This app is built on top of the ZF Skeleton Application, version 3.0.3-dev.
  
  We are using a single controller with two endpoints and a class implemented through an interface called
  Operation.
 
  For decoupling purposes, the controller gets invoked through a factory where we provision and define the child Operation class corresponding to the operator selected. 
  
  The factory also prepares the posted data through the base class constructor.
  
  The operation->calculate method is then invoked by an ajax call.
 
 
 We may easily add more operands modiying the Operation class constructor or more operations (Factorial, Modulo, Exponentiation) by adding more children operation classes in the Application/Operation namespace.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

To run this app you will need:

- Docker
- Web Browser

### Installing

From a console terminal run:
```
git clone git@bitbucket.org:fschemb/level5.git
cd level5
composer install
docker-compose up -d
```
## Deployment

Once installed, go to to 
```
http://10.5.0.6 
```
and try any combination of operands and operators.
Enjoy. 

## Testing

Tests may be found at
```
module/Application/test/Controller/CalculatorTest.php
```

## Built With

* [Zend Framework](https://framework.zend.com/) - The PHP web framework used
* [Composer](https://getcomposer.org/) - Dependency Management
* [Boostrap](https://getbootstrap.com/) - Front-end component library

