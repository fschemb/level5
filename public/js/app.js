/**
 *  Submit form
 */
$("#calculator-form").submit(function (e) {
    //Do not refresh page
    e.preventDefault();
    //Post to controller
    $.ajax({
        type: 'POST',
        url: 'post',
        data: $("#calculator-form").serialize(),
        success: function (data) {
            console.log(data);
            $(".result").css("display", "block");
            $(".result").html("Your result is " + data.result);
        }
    });
});