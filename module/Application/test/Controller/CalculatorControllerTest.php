<?php
declare(strict_types=1);
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ApplicationTest\Controller;

use Application\Controller\IndexController;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;


class CalculatorControllerTest extends TestCase
{
    protected $client;
    protected $baseUri = 'http://10.5.0.6';

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = new Client([
            'base_uri' => $this->baseUri
        ]);
    }

    public function testIndexActionCanBeAccessed()
    {
        $response = $this->client->get('/');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testAddition()
    {
        $response = $this->client->post('/post', [
            'form_params' => [
                'operand1' => 1,
                'operand2' => 1,
                'operator' => 'addition'
            ]
        ]);

        $data = json_decode($response->getBody()->getContents(), true);
        $this->assertEquals(2, $data['result']);
    }

    public function testSubtraction()
    {
        $response = $this->client->post('/post', [
            'form_params' => [
                'operand1' => 10,
                'operand2' => 5,
                'operator' => 'subtraction',
            ]
        ]);

        $data = json_decode($response->getBody()->getContents(), true);
        $this->assertEquals(5, $data['result']);
    }

    public function testMultiplication()
    {
        $response = $this->client->post('/post', [
            'form_params' => [
                'operand1' => 2,
                'operand2' => 2,
                'operator' => 'multiplication',
            ]
        ]);

        $data = json_decode($response->getBody()->getContents(), true);
        $this->assertEquals(4, $data['result']);
    }

    public function testDivision()
    {
        $response = $this->client->post('/post', [
            'form_params' => [
                'operand1' => 2,
                'operand2' => 2,
                'operator' => 'division',
            ]
        ]);

        $data = json_decode($response->getBody()->getContents(), true);
        $this->assertEquals(1, $data['result']);
    }
}
