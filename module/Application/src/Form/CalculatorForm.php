<?php
declare(strict_types=1);

namespace Application\Form;

use Zend\Form\Form;

/**
 * Class CalculatorForm
 * @package Application\Form
 */
class CalculatorForm extends Form
{

    protected $captcha;

    public function __construct()
    {
        parent::__construct();

        $this->add([
            'name' => 'operand1',
            'type'  => 'number',
            'attributes' => [
                'required' => true,
                'class' => 'form-control',
                'style' => 'width:200px;'
            ]
        ]);

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'operator',
            'options' => [
                'value_options' => [
                    'addition' => html_entity_decode('&#x1F480; +'),
                    'subtraction' => html_entity_decode('&#x1F47B; -'),
                    'multiplication' => html_entity_decode('&#x1F47B; x'),
                    'division' => html_entity_decode('&#x1F631; /'),
                ],
            ],
            'attributes' => [
                'required' => true,
                'class' => 'form-control',
                'style' => 'width:100px;'
            ]
        ));

        $this->add([
            'name' => 'operand2',
            'type'  => 'number',
            'attributes' => [
                'required' => true,
                'class' => 'form-control',
                'style' => 'width:200px;'
            ]
        ]);

        $this->add([
            'name' => 'calculate',
            'type'  => 'Submit',
            'attributes' => [
                'value' => 'Calculate',
            ],
        ]);

    }
}
