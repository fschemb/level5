<?php
declare(strict_types=1);

namespace Application\Operation;


class Addition extends Operation
{
    protected $operands;

    public function calculate()
    {
        return $this->operands[0] + $this->operands[1];
    }
}