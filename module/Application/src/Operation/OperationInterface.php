<?php
declare(strict_types=1);

namespace Application\Operation;

/**
 * Interface OperationInterface
 * @package Application\Controller
 */
interface OperationInterface
{
    public function __construct($post);
}