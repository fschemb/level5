<?php
declare(strict_types=1);

namespace Application\Operation;

class Operation implements OperationInterface
{
    protected $operands;

    public function __construct($post)
    {
        // You may add more operands here
        $operands = [
            $post['operand1'],
            $post['operand2']
        ];

        $this->operands = $operands;
    }
}