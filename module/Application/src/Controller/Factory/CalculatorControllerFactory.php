<?php
declare(strict_types=1);

namespace Application\Controller\Factory;

use Application\Controller\CalculatorController;
use Application\Operation\Operation;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Operation\OperationInterface;
use Application\Operation\Addition;

/**
 * @package Application
 * @method OperationInterface calculateResult
 */
class CalculatorControllerFactory implements FactoryInterface {

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null) {

        $class = new $requestedName();

        if(!($class instanceof CalculatorController)) {
            throw new \Exception($requestedName . ' not an instance of ' . CalculatorController::class);
        }

        // Set type of operation
        $post = $_POST;
        if (!empty($post)) {
            $className = ucfirst($post['operator']);
            $operationClass = "Application\Operation\\$className";
            $operation = new $operationClass($post);
            $class->setOperation($operation);
        }

        return $class;
    }

}
