<?php
declare(strict_types=1);

namespace Application\Controller;

use Application\Form\CalculatorForm;
use Zend\Mvc\Controller\AbstractActionController;


/**
 * Class CalculatorController
 * @package Application\Controller
 */
class CalculatorController extends AbstractActionController
{
    protected $operation;

    public function indexAction()
    {
        return ['form' => new CalculatorForm()];
    }

    public function postAction()
    {
        $postData = $this->params()->fromPost();
        $result = [];
        if (!empty($postData)) {
            $result = $this->operation->calculate();
        }
        //Display json
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        $response->setContent(json_encode(['result' => $result]));

        return  $response;
    }

    public function setOperation($operation) {
        $this->operation = $operation;
    }

}